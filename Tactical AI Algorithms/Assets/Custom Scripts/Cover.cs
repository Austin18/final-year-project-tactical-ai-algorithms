﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

namespace TacticalAI.AI
{
    public class Cover : MonoBehaviour
    {

        // Use this for initialization
        public CoverGenerator coverGenerator;
        public bool inCover;
        public Unit unit;
        public List<GameObject> coverTargets;
        protected GameObject closestCover;
        float? dist;


        private void Awake()
        {
            inCover = false;
            

        }

       public void setPathFindTarget()
        {

            Stopwatch sw = new Stopwatch();
            sw.Start();
            unit.test = null;
                unit.coverNode = null;
                closestCover = null;
                dist = null;

                foreach (GameObject go in coverGenerator.coverPoints)
                {

                    float? tempDist;
                    tempDist = Vector3.Distance(unit.transform.position, go.transform.position);
                    if (dist == null)
                    {
                        dist = tempDist;

                    }

                    if (tempDist <= dist)
                    {
                        dist = tempDist;
                        closestCover = go;
                    }
                }
                unit.test = closestCover;
                unit.coverNode = closestCover;
                unit.NewPath();

            sw.Stop();
            print("CoverFound:" + sw.ElapsedMilliseconds + "ms");


        }

        // Update is called once per frame
        void Update()
        {

            if (Input.GetButtonDown("Jump"))
            {
                inCover = true;
            }

          /*  if (inCover == true)
            {
                inCover = false;
                setPathFindTarget();
            }*/

        }
    }
}