﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;


public class CoverGenerator : MonoBehaviour {

    float circumference;
    public EnemyTurret enemyturret;
    public List<EnemyTurret> enemyTurret;
    float coverCheckRange;
    protected int layerMask;
    public Grid grid;
    public List<Node> potentialCover;
    public int nodeCount;
    public GameObject coverTest;
    public List<GameObject> coverPoints;


    // Use this for initialization
    void Start ()
    {
        potentialCover = new List<Node>();
        grid = GameObject.FindWithTag("A*").GetComponent<Grid>();
        coverCheckRange = enemyturret.turretRange;
        layerMask = 1 << LayerMask.NameToLayer("Unwalkable");
        generateCoverSpots();
        
    }

    public void generateCoverSpots()
    {

        Stopwatch sw = new Stopwatch();
        sw.Start();
      

        foreach (Node n in grid.grid)
        {
            for (int i = 0; i < enemyTurret.Count; i++)
            {
                if ((Physics.CheckSphere(n.worldPosition, 1.5f, grid.unwalkableMask)) && n.walkable == true && Vector3.Distance(n.worldPosition, enemyTurret[i].transform.position) < enemyTurret[i].turretRange)
                {
                    potentialCover.Add(n);
                }
            }
        }



        foreach (Node n in potentialCover)
        {
            for (int i = 0; i < enemyTurret.Count; i++)
            {
                if (Physics.Linecast(enemyTurret[i].transform.position, n.worldPosition, grid.unwalkableMask) && Vector3.Distance(n.worldPosition, enemyTurret[i].transform.position) < enemyTurret[i].turretRange)
                {
                    n.coverNode = true;

                    //   temp.position = n.worldPosition;

                    GameObject temp = Instantiate(coverTest, n.worldPosition, Quaternion.identity);
                    coverPoints.Add(temp);


                }
            }
            
        }


        nodeCount = potentialCover.Count;
        sw.Stop();
        print("CoverGenerated:" + sw.ElapsedMilliseconds + "ms");

    }
    
    
	

}
