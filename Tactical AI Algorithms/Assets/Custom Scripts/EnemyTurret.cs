﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class EnemyTurret : MonoBehaviour
{
    public float health;
    public float turretRange;
    public Vector3 turretPosition;
    public List<GameObject> Soldiers;
    public GameObject[] tempSoldiers;
    protected float EnemyDistance;
    public List<GameObject> soldiersInRange;
    protected Vector3 Target;
    protected int layerMask;
    protected Quaternion targetrotation;
   protected GameObject TargetSoldier;
    protected float? targethealth;
    public bool destroyed;
    public GameObject turretProjectile;
    public Transform firePoint;


    // Use this for initialization
    void Start()
    {

        health = 1200;
        layerMask = 1 << LayerMask.NameToLayer("Unwalkable");
        turretRange = 25.0f;
        turretPosition = transform.position;
        tempSoldiers = GameObject.FindGameObjectsWithTag("Soldier");
        for (int i = 0; i < tempSoldiers.Length; i++)
        {
            Soldiers.Add(tempSoldiers[i]);
        }

        StartCoroutine("update");
        targethealth = null;

    }

    private IEnumerator update()
    {
        while (true)
        {
            if (health > 0)
            {
                foreach (GameObject go in Soldiers)
                {
                    EnemyDistance = Vector3.Distance(turretPosition, go.transform.position);

                    if (EnemyDistance <= turretRange && !soldiersInRange.Contains(go))
                    {
                        soldiersInRange.Add(go);
                        if (!go.GetComponent<Unit>().enemiesInView.Contains(gameObject))
                        {
                            go.GetComponent<Unit>().enemiesInView.Add(gameObject);
                            print("enemiesinview" + go.GetComponent<Unit>().enemiesInView[0]);

                        }


                    }
                    else if (EnemyDistance > turretRange && soldiersInRange.Contains(go))
                    {
                        soldiersInRange.Remove(go);
                        if (go.GetComponent<Unit>().enemiesInView.Contains(gameObject))
                        {
                            go.GetComponent<Unit>().enemiesInView.Remove(gameObject);
                        }
                    }
                }

                foreach (GameObject go in soldiersInRange)
                {
                    float tempHealth = go.GetComponent<Unit>().health;

                    if (targethealth == null)
                    {
                        targethealth = tempHealth;
                        TargetSoldier = go;
                    }

                    if (tempHealth < targethealth)
                    {
                        targethealth = tempHealth;
                        TargetSoldier = go;
                    }

                }
                if (TargetSoldier != null)
                {
                    Vector3 forward = firePoint.TransformDirection(Vector3.forward);
                    Vector3 toOther = TargetSoldier.transform.position - transform.position;
                    float dotcheck = Vector3.Dot(forward, toOther.normalized);
                    if (dotcheck > 0.9f && TargetSoldier.GetComponent<Unit>().inCover == false)
                    {
                        GameObject go = Instantiate(turretProjectile);
                        go.transform.position = firePoint.position;
                        go.transform.rotation = Quaternion.LookRotation(TargetSoldier.transform.Find("AimPoint").gameObject.transform.position - firePoint.position);
                    }
                }
            }

            yield return new WaitForSeconds(0.4f);
        }
    }


    private void Update()
    {
        if (destroyed != true)
        {
            if (TargetSoldier != null)
            {
                if (!Physics.Linecast(gameObject.transform.position, TargetSoldier.transform.position, layerMask))
                {
                    TargetSoldier.GetComponent<Unit>().inCover = false;
                    Target = TargetSoldier.transform.Find("AimPoint").gameObject.transform.position;
                    targetrotation = Quaternion.LookRotation(Target - turretPosition);
                    transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, targetrotation, Time.deltaTime * 0.2f);
                }
                else
                {
                    TargetSoldier.GetComponent<Unit>().inCover = true;
                }
                
            }
            if (health <= 0)
            {
                foreach (GameObject go in soldiersInRange)
                {
                    go.GetComponent<Unit>().enemiesInView.Remove(gameObject);
                }
                destroyed = true;
            }
        }
    }
}



    




