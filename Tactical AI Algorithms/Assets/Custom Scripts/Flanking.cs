﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class Flanking : MonoBehaviour {


    protected float flankingDistance;
    protected float startdistance;
    public bool flank;
    public EnemyTurret enemyTurret;
    public Grid grid;
    public GameObject flankTest;
    public Unit unit;
   public List<GameObject> flankpoints;
    float? dist;
    GameObject closestFlankPoint;
    // Use this for initialization
    void Start () {
        flankingDistance = 10;
        startdistance = 8;
        
        flank = false;
        grid = GameObject.FindWithTag("A*").GetComponent<Grid>();
        unit = gameObject.GetComponent<Unit>();
        

        flankpoints = new List<GameObject>();
         
    }

   public void FindFlankPoint()
    {
        if (flank == false)
        {

            Stopwatch sw = new Stopwatch();
            sw.Start();
            flankpoints.Clear();
            unit.test = null;
            Destroy(closestFlankPoint);
            closestFlankPoint = null;
            dist = null;
            if (unit.enemiesInView.Count > 0)
            {
                enemyTurret = unit.enemiesInView[0].GetComponent<EnemyTurret>();
                foreach (Node n in grid.grid)
                {
                    GameObject temp;
                    Vector3 forward = enemyTurret.transform.TransformDirection(Vector3.forward);
                    Vector3 toOther = n.worldPosition - enemyTurret.transform.position;
                    float dotcheck = Vector3.Dot(forward, toOther.normalized);
                    if (dotcheck < -0.95f && Vector3.Distance(n.worldPosition, enemyTurret.transform.position) < flankingDistance && dotcheck < -0.707f && Vector3.Distance(n.worldPosition, enemyTurret.transform.position) > startdistance)
                    {
                        temp = Instantiate(flankTest, n.worldPosition, Quaternion.identity);
                        flankpoints.Add(temp);
                    }
                }

                foreach (GameObject go in flankpoints)
                {

                    float? tempDist;
                    tempDist = Vector3.Distance(go.transform.position, unit.transform.position);
                    if (dist.HasValue == false)
                    {
                        dist = tempDist;

                    }

                    if (tempDist <= dist)
                    {
                        dist = tempDist;
                        closestFlankPoint = go;
                    }
                }

                unit.test = closestFlankPoint;
                unit.NewPath();

                Removeflankpoints();

                sw.Stop();
                print("FlankFound:" + sw.ElapsedMilliseconds + "ms");


            }
           
        }

    }

    public void Removeflankpoints()
    {
        foreach( GameObject go in flankpoints)
        {
            if (go != closestFlankPoint)
            {
                Destroy(go);
            }
        }

        flankpoints.Clear();
        flank = true;
    }


	
	// Update is called once per frame
	void Update () 
            {
     /*   if(flank == true)
        {
            FindFlankPoint();
            flank = false;
        }*/

        if (Input.GetButtonDown("Jump") & unit.inCover == true)
        {
            flank = true;
        }
    }
}
