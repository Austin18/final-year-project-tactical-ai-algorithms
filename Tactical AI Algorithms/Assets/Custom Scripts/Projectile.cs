﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public float speed;
    public int damage = 10;
    Transform selftransform;

    void Update()
    {
        transform.position = transform.position + (transform.forward * speed * Time.deltaTime);
       
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }


     

    void OnTriggerEnter(Collider other)
    {
       

        Vector3 forward = other.gameObject.transform.TransformDirection(Vector3.forward);
        Vector3 toOther = transform.position - other.gameObject.transform.position;
        float dotcheck = Vector3.Dot(forward, toOther.normalized);
        if (other.tag == "Enemy" && dotcheck < 0  )
        {
            print("Damage Done:" + damage * 2 + " Points Of Damage");
            other.GetComponent<EnemyTurret>().health -= damage * 2;
            Destroy(gameObject);
            

        }
        else if (other.tag == "Enemy")
        {
            print("Damage Done:" + damage + " Points Of Damage");
            other.GetComponent<EnemyTurret>().health -= damage;
            Destroy(gameObject);
        }
    }
}
