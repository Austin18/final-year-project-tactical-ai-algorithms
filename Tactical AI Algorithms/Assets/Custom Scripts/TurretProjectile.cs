﻿using UnityEngine;
using System.Collections;

public class TurretProjectile : MonoBehaviour
{
    public float speed;
    public int damage = 10;
    Transform selftransform;

    void Update()
    {
        transform.position = transform.position + (transform.forward * speed * Time.deltaTime);

    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }




    void OnTriggerEnter(Collider other)
    {


        Vector3 forward = other.gameObject.transform.TransformDirection(Vector3.forward);
        Vector3 toOther = transform.position - other.gameObject.transform.position;
        float dotcheck = Vector3.Dot(forward, toOther.normalized);
        if (other.tag == "Soldier" && dotcheck < 0)
        {
            float tempDamage = damage * 2;
            other.GetComponent<Unit>().TakeDamage(tempDamage);
            Destroy(gameObject);

        }
        else if (other.tag == "Soldier")
        {
            other.GetComponent<Unit>().TakeDamage(damage);
            Destroy(gameObject);
        }
    }
}
