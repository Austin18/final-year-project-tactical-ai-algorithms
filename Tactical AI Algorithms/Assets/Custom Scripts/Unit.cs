﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TacticalAI.AI;
using UnityEngine.SceneManagement;



public class Unit : MonoBehaviour
{
    public delegate string aiHandler();
    protected Dictionary<string, aiHandler> aiActions;

    [SerializeField]
    protected string _state;

    protected string prevState;

    public float AITickRate;

    public string state
    {
        get { return _state; }

        set
        {
            _state = value;

            if (_state != prevState)
            {
                if (_state != null)
                {
                    if (prevState != null)
                    {
                        // D.log("AI", "AI for " + structure.gameObject.name + " has changed state from " + prevState + " to " + _state);
                    }
                    else
                    {
                        // D.log("AI", "AI for " + structure.gameObject.name + " has an initial state of " + _state);
                    }
                }
                else
                {
                    // D.log("AI", "AI for " + structure.gameObject.name + " has a null state");
                }

                prevState = _state;
            }
        }
    }

    public GameObject test;
    public Transform target;
    public float speed = 20;
    Vector3[] path;
    int targetIndex;
    bool haschanged;
    public bool inCover;
    float attackRange;
    protected float viewRange;
    public List<GameObject> enemiesInView;
    public GameObject targetenemy;
    public GameObject coverNode;
    public float health;
    public float previousHealth;
    public GameObject[] enemies;
    public Cover cover;
    public Flanking flank;
    public GameObject projectile;
    public Transform firepoint;
    public int enemiesdestroyed;
    public GameObject finalPoint;
        
    
        




    void Start()
    {
        cover = gameObject.GetComponent<Cover>();
        flank = gameObject.GetComponent<Flanking>();

        aiActions = new Dictionary<string, aiHandler>();

        enemiesInView = new List<GameObject>();

        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        aiActions.Add("MOVETOTARGET", moveToTargetAction);
        aiActions.Add("SEEKCOVER", seekCoverAction);
        aiActions.Add("FLANKTARGET", flankTargetAction);
        aiActions.Add("COMBAT", combatAction);

        finalPoint = GameObject.FindGameObjectWithTag("EndPoint");

        attackRange = 15;
        viewRange = 30;

        NewPath();
      

        InvokeRepeating("update", 0, AITickRate);
        inCover = false;
        state = "MOVETOTARGET";
    }

    public void update()
    {
        if(targetenemy!= null && targetenemy.GetComponent<EnemyTurret>().destroyed == true)
        {
            state = "MOVETOTARGET";
            flank.flank = false;
            cover.inCover = false;
            targetenemy = null;
        }

      

        processState();
       // yield return null;
    }

    public void processState()
    {
        if (state != null)
        {
            if (!aiActions.ContainsKey(state))
            {
                // D.log("AI", "AI for " + structure.gameObject.name + " has no state called " + state + " in its controller");

                // no matching key
                state = null;
                return;
            }

            aiHandler handler = aiActions[state];

            if (handler != null)
            {
                // // D.log("AI", "Processing handler for state: " + state);
               state = handler();
            }
            else
            {
                // D.log("AI", "AI for " + structure.gameObject.name + " has no state handler for state called " + state + " in its controller");
                state = null;
            }
        }
    }

    public void NewPath()
    {

        if (test != null)
        {
            target = test.transform;
            path = new Vector3[0];
            PathRequestManager.RequestPath(transform.position, target.position, OnPathFound);
        }
        else
        { }
    }


    public void OnPathFound(Vector3[] newPath, bool pathSuccessful)
    {
        if (pathSuccessful)
        {
            path = newPath;
            StopCoroutine("FollowPath");
            StartCoroutine("FollowPath");
        }
    }

    IEnumerator FollowPath()
    {

        Vector3 currentWaypoint;
        if (path.Length <= 0)
        {
            //   NewPath();
        }
        else
        {
            currentWaypoint = path[0];


            while (true)
            {
                if (transform.position == currentWaypoint)
                {
                    targetIndex++;
                    if (targetIndex >= path.Length)
                    {

                        targetIndex = 0;
                        //NewPath();

                        yield break;
                    }


                    currentWaypoint = path[targetIndex];

                }
                transform.rotation = Quaternion.LookRotation(currentWaypoint - transform.position);
                transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, speed * Time.deltaTime);
                yield return null;
            }
        }
    }

   public string moveToTargetAction()
    {

        enemiesdestroyed = 0;
        for(int i = 0; i < enemies.Length; i++)
        {
            if(enemies[i].GetComponent<EnemyTurret>().destroyed != true)
            {
                test = enemies[i];
                targetenemy = enemies[i];
                NewPath();  
                break;
            }
            else
            {
                enemiesdestroyed++;
            }
        }
        if(enemiesdestroyed == enemies.Length)
        {
            test = finalPoint;
            NewPath();
            if(Vector3.Distance(transform.position, test.transform.position) < 2)
            {
                SceneManager.LoadScene("TacticalAIScene2");
            }

        }
        if (targetenemy != null)
        {
            if (Vector3.Distance(transform.position, targetenemy.transform.position) < 25)
            {
                cover.inCover = false;
                return "SEEKCOVER";
            }
            else
            {
                return "MOVETOTARGET";
            }
        }
        else
        {
            return "MOVETOTARGET";
        }

        
    }

    public string seekCoverAction()
    {
        
        
            cover.setPathFindTarget();
        
      
            if (Vector3.Distance(transform.position, test.transform.position) < 2 && flank != null)
            {

                cover.inCover = false;
                flank.flank = false;

                return "FLANKTARGET";
            }
            else
            {
                cover.inCover = false;
                flank.flank = false;
                return "SEEKCOVER";
            }
    }
        
    

    public string flankTargetAction()
    {
       // flank.flank = false;
        flank.FindFlankPoint();
       
        if (Vector3.Distance(transform.position, target.position) < 2 && flank.flank == true)
        {

            flank.flank = true;
            return "COMBAT";
            
        }
        else
        {
            return "FLANKTARGET";
        }

    }

    public string combatAction()
    {
        if (enemiesInView.Count > 0)
        {
            transform.rotation = Quaternion.LookRotation(enemiesInView[0].transform.position - transform.position);
            // transform.rotation = Quaternion.Euler(0, transform.rotation.y, transform.rotation.z);
            GameObject go = Instantiate(projectile);
            go.transform.position = firepoint.position;
            go.transform.rotation = Quaternion.LookRotation(enemiesInView[0].transform.position - firepoint.position);
        }
        else
        {
            return "MOVETOTARGET";
        }
        if (health < previousHealth)
        {
            previousHealth = health;
            return "SEEKCOVER";
        }
        else
        {
            return "COMBAT";
        }

    }

    public void OnDrawGizmos()
    {
        if (path != null)
        {
            for (int i = targetIndex; i < path.Length; i++)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(path[i], Vector3.one);
                if (i == targetIndex)
                {
                    Gizmos.DrawLine(transform.position, path[i]);
                }
                else
                {
                    Gizmos.DrawLine(path[i - 1], path[i]);
                }
            }
        }
    }

   public void TakeDamage(float damage)
    {
        previousHealth = health;
        health -= damage;
        //state = "SEEKCOVER";
       
    }

    private void Update()
    {
        if (test != null && test.transform.hasChanged)
        {
            test.transform.hasChanged = false;
            haschanged = true;

            if (haschanged == true)
            {
                haschanged = false;
                NewPath();
            }
        }
        //  if(transform.position)

      //  print("enemiesinview" + enemiesInView.Count);
    }
   


}

